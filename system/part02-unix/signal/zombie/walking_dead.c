#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void shortgun(int sig)
{
	printf("Child is dead\n");
	int status;
	int pid = wait(&status);
	printf("Done killing the zombie: PID: %d, status: %d\n", pid, status);
	if (WIFEXITED(status)) {
		printf("Exit status: %d\n", WEXITSTATUS(status));
	} else {
		printf("Signaled: %d\n", status);
	}
}

void kill_zombie()
{
	struct sigaction act;
        memset(&act, 0, sizeof(act));
	act.sa_handler = shortgun;
        sigaction(SIGCHLD, &act, 0);
}

int main()
{
	pid_t pid = fork();
	if (pid > 0) {
		kill_zombie();
		for (;;)
			sleep(1);
	} else {
		printf("CHILD!!!\n");
		return 2;
	}
}
