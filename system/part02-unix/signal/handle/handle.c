#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <execinfo.h>
#include <signal.h>

void add_handler(int sig, void handler(int))
{
	struct sigaction act;
	memset(&act, 0, sizeof(act));
	act.sa_handler = handler;
	sigaction(sig, &act, 0);
}

void print_stack_trace()
{
	int size = 20;
	void *array[size];
	size_t stack_size;
	char **strings;
	size_t i;

	stack_size = backtrace(array, size);
	strings = backtrace_symbols(array, stack_size);

	for (i = 0; i < stack_size; i++)
		printf("%s\n", strings[i]);

	free(strings);
}

void segfault_handler(int sig)
{
	printf("SEGFAULT!!!\n");
	print_stack_trace();
	exit(3);
}

void abort_handler(int sig)
{
	printf("Aborted!!!\n");
	print_stack_trace();
}

int main()
{
	add_handler(SIGSEGV, segfault_handler);
	add_handler(SIGABRT, abort_handler);
	int *x = 0;
	printf("%d\n", *x);
	for (;;) {
		sleep(1);
	}
}
