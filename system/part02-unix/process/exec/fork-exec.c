#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{	
	pid_t pid = fork();
	if (pid > 0) {
		printf("This is parent\n");
		int ret;
		waitpid(pid, &ret, 0);
	} else {
		char *child_args[3];
		child_args[0] = "child";
		child_args[1] = "Dad";
		child_args[2] = NULL;
		execve("child", child_args, NULL);
	}
}
