#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
	char buffer[255];
	int size = read(STDIN_FILENO, buffer, 255);
	write(STDOUT_FILENO, "Hello, ", 7);
	write(STDOUT_FILENO, buffer, size);
	write(STDOUT_FILENO, "\n", 1);
}
