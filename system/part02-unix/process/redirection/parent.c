#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

int main()
{
	pid_t pid = fork();
	if (pid == 0) {
		int input = open("input.txt", O_RDONLY);
		dup2(input, STDIN_FILENO);
		close(input);
		int output = open("output.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
		dup2(output, STDOUT_FILENO);
		close(output);
		execve("hello", NULL, NULL);
	} else {
		int ret;
		waitpid(pid, &ret, 0);
	}
}
