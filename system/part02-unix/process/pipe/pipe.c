#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

int main()
{
	pid_t pid = fork();
	if (pid == 0) {
		int pipefd[2];
		pipe(pipefd);
		int pid2 = fork();
		if (pid2 > 0) {
			int input = open("input.txt", O_RDONLY);
			dup2(input, STDIN_FILENO);
			close(input);
			dup2(pipefd[1], STDOUT_FILENO);
			close(pipefd[0]);
			close(pipefd[1]);
			char *args[2];
			args[0] = "/bin/sort";
			args[1] = NULL;
			execve("/bin/sort", args, NULL);
		} else {
			dup2(pipefd[0], STDIN_FILENO);
			close(pipefd[0]);
                        close(pipefd[1]);
			char *args[2];
                        args[0] = "/usr/bin/uniq";
			args[1] = NULL;
			execve("/usr/bin/uniq", args, NULL);
		}
	} else {
		int ret;
		waitpid(pid, &ret, 0);
	}
}
