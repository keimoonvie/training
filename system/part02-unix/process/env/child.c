#include <stdio.h>

int main(int argc, char *argv[], char *envp[])
{
	int i =0;
        char *env = envp[i];
	printf("=============== ENV BEGIN ================\n");
        while (env != NULL) {
                printf("%s\n", env);
		i++;
		env = envp[i];
	}
        printf("=============== ENV END ================\n");
	if (argc == 1) {
		printf("Hello, world\n");
	} else {
		printf("Hello, %s\n", argv[1]);
	}
}
