#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[], char *envp[])
{
	pid_t pid = fork();
	int i =0;
	char *env = envp[i];
	printf("=============== ENV BEGIN ================\n");
        while (env != NULL) {
		printf("%s\n", env);
		i++;
		env = envp[i];
        }
	printf("=============== ENV END ================\n");
	if (pid > 0) {
		printf("This is parent\n");
	} else {
		char *child_args[3];
		child_args[0] = "child";
		child_args[1] = "Dad";
		child_args[2] = NULL;
		execve("child", child_args, NULL);
	}
}
