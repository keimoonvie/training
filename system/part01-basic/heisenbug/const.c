#include <stdio.h>

int main()
{
	const int x = 5;
	int *y = (int *) &x;
	*y = 10;
	printf("Pointer x: %p, Value x: %d\n", &x, x);
	printf("Pointer y: %p, Value y: %d\n", y, *y);
}
