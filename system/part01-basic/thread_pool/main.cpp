#include <iostream>
#include <cstdlib>
#include "thread_pool.h"

using namespace std;
using namespace example;

ThreadPool pool(10);

class Job: public Runnable
{
public:
	Job(int x): x(x)
	{
	};

	void run()
	{
		cout << x << endl;
	}
private:
	int x;
};

class Publisher: public Runnable
{
public:
        void run()
        {
                string in;
                for (;;) {
			cin >> in;
			int x = atoi(in.c_str());
			if (x > 0) {
                                pool.run(new Job(x));
			} else {
                                pool.close();
				break;
			}
		}
	};
};

int main()
{
	pool.start();
	Thread t(new Publisher());
	t.start();
	t.join();
	pool.join();
}
