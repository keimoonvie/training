#include "queue.h"

using namespace std;

namespace example
{
	Queue::Queue()
        {
                pthread_mutex_init(&mutex, NULL);
                pthread_cond_init(&cond, NULL);
                running = true;
        };

        Queue::~Queue()
        {
                pthread_mutex_destroy(&mutex);
                pthread_cond_destroy(&cond);
        }

        void Queue::enqueue(void *data)
        {
                pthread_mutex_lock(&mutex);
                l.push_back(data);
                pthread_cond_signal(&cond);
                pthread_mutex_unlock(&mutex);
        }

	void *Queue::dequeue()
        {
                pthread_mutex_lock(&mutex);
		if (!running) {
			pthread_mutex_unlock(&mutex);
			return NULL;
		}
                while (l.size() == 0) {
                        pthread_cond_wait(&cond, &mutex);
                        if (!running) {
                                pthread_mutex_unlock(&mutex);
                                return NULL;
                        }
                }
                void *data = l.front();
                l.pop_front();
                pthread_mutex_unlock(&mutex);
                return data;
        }

	void Queue::close()
        {
                pthread_mutex_lock(&mutex);
                if (running) {
                        running = false;
                        pthread_cond_broadcast(&cond);
                }
                pthread_mutex_unlock(&mutex);
        }
}
