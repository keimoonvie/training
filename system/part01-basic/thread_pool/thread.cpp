#include "thread.h"

namespace example
{
	void *_thread_cb(void *ptr)
	{
		Thread *thread = (Thread *) ptr;
		thread->run();
	}

	Thread::Thread(Runnable *runner): runner(runner)
	{
        }

        Thread::~Thread()
	{
		if (runner)
                        delete runner;
	}

        void Thread::start()
	{
		pthread_create(&thread, NULL, _thread_cb, this);
	}

        void Thread::join()
	{
		pthread_join(thread, NULL);
	}

        void Thread::run()
	{
		runner->run();
	}
}
