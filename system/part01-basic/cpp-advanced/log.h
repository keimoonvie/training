#ifndef LOG_H_
#define LOG_H_

#include <cstdio>
#include <cstdarg>

namespace example
{
	class Logger
	{
	public:
		enum LogLevel
		{
			DEBUG, INFO, WARNING, ERROR, FATAL,
		};

		Logger(LogLevel level = INFO);
		virtual ~Logger();

		void debug(const char * file, int line, const char *format, ...);
		void info(const char * file, int line, const char *format, ...);
		void warning(const char * file, int line, const char *format, ...);
		void error(const char * file, int line, const char *format, ...);
		void fatal(const char * file, int line, const char *format, ...);
	protected:
		virtual void vlog(const char * file, int line, LogLevel level, const char *format, va_list ap) = 0;

		static const char *log_level_string[];
		LogLevel level;
		bool verbose;
	};

	class StdLogger: public Logger
	{
	public:
		StdLogger(LogLevel level = INFO);
	protected:
		void vlog(const char * file, int line, LogLevel level, const char *format, va_list ap);
	};

	class FileLogger: public Logger
	{
	public:
		FileLogger(const char *logfile, LogLevel level = INFO);
		virtual ~FileLogger();
		bool init();
	protected:
                void vlog(const char * file, int line, LogLevel level, const char *format, va_list ap);
		const char *logfile;
		FILE *handle;
		int line_count;
		int sequence;
	};

	class Log
	{
	public:
		virtual ~Log();
		static Log & get_instance();

		void set_logger(Logger *logger);
		
		inline Logger *get_logger() const
		{
			return logger;
		}
	private:
		Log();
		Logger *logger;
	};
}

#define LOGGER_SET(l) example::Log::get_instance().set_logger(l)
#define LOGGER_DEBUG(s, ...) example::Log::get_instance().get_logger()->debug(__FILE__, __LINE__, s, ##__VA_ARGS__)
#define LOGGER_INFO(s, ...) example::Log::get_instance().get_logger()->info(__FILE__, __LINE__, s, ##__VA_ARGS__)
#define LOGGER_WARNING(s, ...) example::Log::get_instance().get_logger()->warning(__FILE__, __LINE__, s, ##__VA_ARGS__)
#define LOGGER_ERROR(s, ...) example::Log::get_instance().get_logger()->error(__FILE__, __LINE__, s, ##__VA_ARGS__)
#define LOGGER_FATAL(s, ...) example::Log::get_instance().get_logger()->fatal(__FILE__, __LINE__, s, ##__VA_ARGS__)

#endif
