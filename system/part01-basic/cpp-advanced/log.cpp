#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <time.h>
#include "log.h"

namespace example
{
	Logger::Logger(LogLevel level): level(level)
        {
        }

	Logger::~Logger()
	{
        }

	void Logger::debug(const char * file, int line, const char *format, ...)
	{
		va_list ap;
		va_start(ap, format);
		vlog(file, line, DEBUG, format, ap);
		va_end(ap);
	}

	void Logger::info(const char * file, int line, const char *format, ...)
	{
		va_list ap;
		va_start(ap, format);
		vlog(file, line, INFO, format, ap);
		va_end(ap);
	}

	void Logger::warning(const char * file, int line, const char *format, ...)
        {
                va_list ap;
		va_start(ap, format);
		vlog(file, line, WARNING, format, ap);
		va_end(ap);
	}

        void Logger::error(const char * file, int line, const char *format, ...)
	{
                va_list ap;
                va_start(ap, format);
                vlog(file, line, ERROR, format, ap);
                va_end(ap);
        }

	void Logger::fatal(const char * file, int line, const char *format, ...)
        {
                va_list ap;
		va_start(ap, format);
                vlog(file, line, FATAL, format, ap);
		va_end(ap);
		abort();
	}

	const char *Logger::log_level_string[] = {
		"DEBUG", "INFO", "WARNING", "ERROR", "FATAL"
	};

	StdLogger::StdLogger(LogLevel level): Logger(level)
	{
	}

	void StdLogger::vlog(const char * file, int line, LogLevel level, const char *format, va_list ap)
	{
		if (level >= this->level) {
			printf("%s\t", log_level_string[level]);
			char *filename = (char *) strrchr(file, '/');
			if (filename == NULL)
				filename = (char *) file;
			else
				filename = filename + 1;
			printf("%s:%d\t", filename, line);
                        vprintf(format, ap);
                        printf("\n");
                        fflush(stdout);
                }
	}

	FileLogger::FileLogger(const char *logfile, LogLevel level): logfile(logfile), Logger(level)
	{
	}

	FileLogger::~FileLogger()
	{
		if (handle != NULL) {
			fclose(handle);
		}
	}

	bool FileLogger::init()
	{
		handle = fopen(logfile, "a+");
		return handle != NULL;
	}

	void FileLogger::vlog(const char * file, int line, LogLevel level, const char *format, va_list ap)
        {
		if (level >= this->level && handle != NULL) {
                        fprintf(handle, "%s\t", log_level_string[level]);
                        char *filename = (char *) strrchr(file, '/');
                        if (filename == NULL)
                                filename = (char *) file;
			else
                                filename = filename + 1;
                        fprintf(handle, "%s:%d\t", filename, line);
			vfprintf(handle, format, ap);
                        fprintf(handle, "\n");
                        fflush(handle);
			line_count++;
			if (line_count >= 10) {
				fclose(handle);
				char new_file[strlen(logfile) + 20];
				sprintf(new_file, "%s.%d.%d", logfile, (int) time(NULL), sequence);
				rename(logfile, new_file);
				handle = fopen(logfile, "a+");
				line_count = 0;
				sequence++;
			}
		}
	}

	Log::Log(): logger(NULL)
	{
	}

	Log & Log::get_instance()
	{
		static Log instance;
		return instance;
	}

	Log::~Log()
	{
		if (logger != NULL)
			delete logger;
	}

	void Log::set_logger(Logger *logger)
	{
		if (this->logger != NULL)
			delete this->logger;
		this->logger = logger;
	}

}
