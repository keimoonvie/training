#include <errno.h>
#include <string.h>
#include "log.h"

using namespace example;

int main()
{
	FileLogger *logger = new FileLogger("test.log", Logger::DEBUG);
	if (!logger->init()) {
		printf("Cannot init logger\n");
		perror("Error");
		return 1;
	}
	LOGGER_SET(logger);
	for (int i = 0; i < 20; i++) {
		LOGGER_DEBUG("test %d", i);
	}
}
