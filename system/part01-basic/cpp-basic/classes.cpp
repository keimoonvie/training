#include <iostream>
#include "classes.h"

using namespace std;

namespace example
{
	IntValue::IntValue(int value): value(value)
	{
		cout << "IntValue constructor: " << value << endl;
	}

	IntValue::~IntValue()
	{
		cout << "IntValue destructor: " << value << endl;
	}

	Person::Person(const string & name, const IntValue & age): name(name), age(age)
	{
		cout << "Person constructor: " << name << ", " << age.get() << endl;
	}

	Person::~Person()
	{
		cout << "Person destructor: " << name << ", " << age.get() << endl;
	}

	void Person::print() const
	{
		cout << name << ", " << age.get() << endl;
	}

	Engineer::Engineer(const string & name, const IntValue & age, const string & job):
		Person(name, age), job(job)

	{
		cout << "Engineer constructor: " << name << ", " << age.get() << ", " << job << endl;
	}

	Engineer::~Engineer()
	{
		cout << "Engineer destructor: " << name << ", " << age.get() << ", " << job << endl;
	}

	void Engineer::print() const
	{
		cout << name << ", " << age.get() << ", " << job << endl;
	}
}
