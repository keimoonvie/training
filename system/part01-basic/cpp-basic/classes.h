#ifndef CLASSES_H_
#define CLASSES_H_

#include <string>

namespace example
{
	class IntValue
	{
	public:
		IntValue(int value = 0);
		virtual ~IntValue();		

		inline int get() const
		{
			return value;
		}
	private:
		int value;
	};

	class Person
	{
	public:
		Person(const std::string & name, const IntValue &age);
		virtual ~Person();

		virtual void print() const;
	protected:
		std::string name;
		IntValue age;
	};

	class Engineer: public Person
	{
	public:
		Engineer(const std::string & name, const IntValue & age, const std::string & job);
		virtual ~Engineer();

		virtual void print() const;
	protected:
		std::string job;
	};
}

#endif
