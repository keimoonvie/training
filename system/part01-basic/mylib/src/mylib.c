#include <stdio.h>
#include "mylib.h"

void hello(const char *name)
{
	printf("Hello, %s from version 3.0.0\n", name);
}
