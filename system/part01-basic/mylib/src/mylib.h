#ifndef MYLIB_H_
#define MYLIB_H_

#ifdef __cplusplus
extern "C" {
#endif

	void hello(const char *name);

#ifdef __cplusplus
}
#endif

#endif
