#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <sys/epoll.h>
#ifdef __FreeBSD__
#include <netinet/in.h>
#endif
#include "net.h"

int socket_new_listener(const char *host, const char *port, int backlog, bool non_blocking, bool no_delay)
{
	int status, socket_fd;
	struct addrinfo hints;
	struct addrinfo *servinfo;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	if ((status = getaddrinfo(host, port, &hints, &servinfo)) != 0) {
		return -1;
	}

	socket_fd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
	if (socket_fd == -1) {
		freeaddrinfo(servinfo);
		return -1;
	}

	int yes = 1;
	if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		freeaddrinfo(servinfo);
		close(socket_fd);
		return -1;
	}

	if (bind(socket_fd, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
		freeaddrinfo(servinfo);
		close(socket_fd);
		return -1;
	}

	if (no_delay) {
		if (!socket_make_no_delay(socket_fd)) {
			freeaddrinfo(servinfo);
			close(socket_fd);
			return -1;
		}
	}

	if (non_blocking) {
		if (!socket_make_non_blocking(socket_fd)) {
			freeaddrinfo(servinfo);
			close(socket_fd);
			return -1;
		}
	}

	if (listen(socket_fd, backlog) == -1) {
		freeaddrinfo(servinfo);
		close(socket_fd);
		return -1;
	}

	freeaddrinfo(servinfo);
	return socket_fd;
}

int socket_new_connection(const char *host, const char *port)
{
	int status, socket_fd;
	struct addrinfo hints;
	struct addrinfo *servinfo, *p;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	if ((status = getaddrinfo(host, port, &hints, &servinfo)) != 0) {
		return -1;
	}

	socket_fd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
	if (socket_fd == -1) {
		freeaddrinfo(servinfo);
		close(socket_fd);
		return -1;
	}

	if (connect(socket_fd, servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
		freeaddrinfo(servinfo);
		close(socket_fd);
		return -1;
	}

	freeaddrinfo(servinfo);
	return socket_fd;
}

bool socket_make_non_blocking(int socket_fd)
{
	int flags, s;
	flags = fcntl(socket_fd, F_GETFL, 0);
	if (flags == -1) {
		return false;
	}

	flags |= O_NONBLOCK;
	return fcntl(socket_fd, F_SETFL, flags) == 0;
}

bool socket_make_no_delay(int socket_fd)
{
	int flag = 1;
	return setsockopt(socket_fd, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int)) == 0;
}

int accept_socket(int server_fd)
{
	struct sockaddr in_addr;
	socklen_t in_len = sizeof(in_addr);
	return accept(server_fd, &in_addr, &in_len);
}

int watch_socket(int epoll_fd, int socket_fd)
{
	struct epoll_event event;
	memset(&event, 0, sizeof(event));
	event.data.fd = socket_fd;
	event.events = EPOLLIN;
	return epoll_ctl(epoll_fd, EPOLL_CTL_ADD, socket_fd, &event);
}
