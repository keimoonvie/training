#ifndef QUEUE_H_
#define QUEUE_H_

#include <list>
#include <pthread.h>

namespace example
{
	class Queue
	{
	public:
		Queue();
		virtual ~Queue();
		void enqueue(void *data);
		void *dequeue();
		void close();
	private:
		std::list<void *> l;
		pthread_mutex_t mutex;
		pthread_cond_t cond;
		bool running;
	};
}

#endif
