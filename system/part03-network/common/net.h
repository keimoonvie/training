#ifndef COMMON_NET_H_
#define COMMON_NET_H_

int socket_new_listener(const char *host, const char *port, int backlog = 256, bool non_blocking = true, bool no_delay = true);
int socket_new_connection(const char *host, const char *port);
bool socket_make_non_blocking(int socket_fd);
bool socket_make_no_delay(int socket_fd);
int accept_socket(int server_fd);
int watch_socket(int epoll_fd, int socket_fd);

#endif
