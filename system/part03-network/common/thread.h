#ifndef THREAD_H_
#define THREAD_H_

#include <pthread.h>

namespace example
{
	class Runnable
	{
	public:
		virtual void run() = 0;
	};

	class Thread
	{
	public:
		Thread(Runnable *runner);
		virtual ~Thread();
		void start();
		void join();
		void run();
	private:
		pthread_t thread;
		Runnable *runner;
	};
}

#endif
