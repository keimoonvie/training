#include <iostream>
#include "thread_pool.h"

using namespace std;

namespace example
{
	ThreadPoolRunner::ThreadPoolRunner(ThreadPool *pool, int id): pool(pool), id(id)
	{
	}

	void ThreadPoolRunner::run()
	{
		for (;;) {
			Runnable *job =(Runnable *) pool->dequeue();
			if (job == NULL)
				break;
			cout << id << " run:" << endl;
			job->run();
			delete job;
                }
	}

	ThreadPool::ThreadPool(int size): size(size)
	{
		for (int i = 0; i < size; i++)
			threads.push_back(new Thread(new ThreadPoolRunner(this, i)));
		queue = new Queue();
	}

	ThreadPool::~ThreadPool()
	{
		for (int i = 0; i < size; i++)
			delete threads[i];
		delete queue;
	}

	void ThreadPool::start()
	{
		for (int i = 0; i < size; i++)
			threads[i]->start();
	}

	void ThreadPool::run(Runnable *runner)
	{
		queue->enqueue(runner);
	}

	void *ThreadPool::dequeue()
	{
		return queue->dequeue();
	}

	void ThreadPool::close()
	{
		queue->close();
	}

	void ThreadPool::join()
	{
		for (int i = 0; i < size; i++)
			threads[i]->join();
	}
}

