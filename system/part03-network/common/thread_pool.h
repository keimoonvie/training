#ifndef THREAD_POOL_H_
#define THREAD_POOL_H_

#include <vector>
#include "thread.h"
#include "queue.h"

namespace example
{
	class ThreadPool;
	class ThreadPoolRunner: public Runnable
	{
	public:
		ThreadPoolRunner(ThreadPool *pool, int id);
		void run();
	private:
		int id;
		ThreadPool *pool;
	};

	class ThreadPool
	{
	public:
		ThreadPool(int size);
		virtual ~ThreadPool();
		
		void start();
		void run(Runnable *runner);
		void close();
		void join();
		void *dequeue();
	private:
		int size;
		std::vector<Thread *> threads;
		Queue *queue;
	};
}

#endif
