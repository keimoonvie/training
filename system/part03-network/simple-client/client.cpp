#include <unistd.h>
#include <iostream>
#include <string>
#include "../common/net.h"

using namespace std;

int main(int argc, char *argv[])
{
	if (argc < 3) {
		cerr << "Usage: " << argv[0] << " [host] [port]" << endl;
		return 2;
	}
	int fd = socket_new_connection(argv[1], argv[2]);
	if (fd < 0) {
		cerr << "Cannot connect" << endl;
		return 1;
	}
	cout << "Connected" << endl;
	while (true) {
		cout << ">> ";
		string str;
		cin >> str;
		if (str == "q") {
			break;
		}
		int written_size = 0;
		while (written_size < str.size()) {
			int size = write(fd, str.data(), str.size());
			written_size += size;
		}
		char buffer[256];
		int size = read(fd, buffer, 255);
		if (size > 0) {
			cout << string(buffer, size) << endl;
		}
	}
	close(fd);
	return 0;
}
