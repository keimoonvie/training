#include <unistd.h>
#include <iostream>
#include "../common/net.h"

using namespace std;

int main(int argc, char *argv[])
{
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " [port]" << endl;
		return 2;
	}
	int server_fd = socket_new_listener("0.0.0.0", argv[1], 256, false);
	if (server_fd < 0) {
		cerr << "Cannot listen" << endl;
		return 1;
	}
	while (true) {
		int fd = accept_socket(server_fd);
		if (fd < 0) {
			cerr << "Cannot accept" << endl;
			return 1;
		}
		cout << "Accepted socket " << fd << endl;
		while (true) {
			char buffer[256];
			int size = read(fd, buffer, 255);
			if (size < 0) {
				cerr << "Read error" << endl;
				close(fd);
				break;
			} else if (size == 0) {
				cout << "Socket closed: " << fd << endl;
				close(fd);
				break;
			} else {
				write(fd, "Hello, ", 7);
				write(fd, buffer, size);
			}
		}
	}
	return 0;
}
