#include <unistd.h>
#include <iostream>
#include <sys/epoll.h>
#include <errno.h>
#include "../common/net.h"

using namespace std;

#define MAX_EVENT 1024

int main(int argc, char *argv[])
{
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " [port]" << endl;
		return 2;
	}
        int server_fd = socket_new_listener("0.0.0.0", argv[1], 256, true);
	if (server_fd < 0) {
		cerr << "Cannot listen" << endl;
		return 1;
	}

	int epoll_fd = epoll_create(1024);
	watch_socket(epoll_fd, server_fd);

	struct epoll_event events[MAX_EVENT];	
	while (true) {
		int n = epoll_wait(epoll_fd, events, MAX_EVENT, -1);
		for (int i = 0; i < n; i++) {
			if (events[i].data.fd == server_fd) {
				while (true) {
					int fd = accept_socket(server_fd);
					if (fd < 0) {
						if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
							cout << "Accept error" << endl;
						}
						break;
					}
					cout << "Accepted socket " << fd << endl;
					socket_make_non_blocking(fd); // Very important!!!
					watch_socket(epoll_fd, fd);
				}
			} else {
				while (true) {
					int fd = events[i].data.fd;
					char buffer[256];
					int size = read(fd, buffer, 255);
					if (size < 0) {
						if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR) {
							cerr << "Read error" << endl;
                                                }
						break;
					} else if (size == 0) {
						cout << "Socket closed: " << fd << endl;
						close(fd);
						break;
					} else {
						write(fd, "Hello, ", 7);
						write(fd, buffer, size);
					}
				}
			}
		}
	}
}
