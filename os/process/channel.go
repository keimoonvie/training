package main

import (
	"fmt"
)

func main() {
	end := make(chan bool, 1)
	c := make(chan int, 20)
	go func() {
		// Publisher
		var x int
		for {
			fmt.Scanf("%d", &x)
			if x == 0 {
				close(c)
				break
			}
			c <- x
		}
	}()

	go func() {
		// Subscriber
		for x := range c {
			if x == 0 {
				break
			}
			fmt.Printf("Got %d\n", x)
		}
		end <- true
	}()
	<-end
}
