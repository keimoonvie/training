#include <iostream>
#include <cstdlib>
#include <list>
#include <pthread.h>

using namespace std;

class Queue
{
public:
	Queue()
	{
		pthread_mutex_init(&mutex, NULL);
		pthread_cond_init(&cond, NULL);
		running = true;
	};

	virtual ~Queue()
	{
		pthread_mutex_destroy(&mutex);
		pthread_cond_destroy(&cond);
	}

	void enqueue(void *data)
	{
		pthread_mutex_lock(&mutex);
		l.push_back(data);
		pthread_cond_signal(&cond);
		pthread_mutex_unlock(&mutex);
	}

	void *dequeue()
	{
		pthread_mutex_lock(&mutex);
		while (l.size() == 0) {
			pthread_cond_wait(&cond, &mutex);
			if (!running) {
				pthread_mutex_unlock(&mutex);
				return NULL;
			}
		}
		void *data = l.front();
		l.pop_front();
		pthread_mutex_unlock(&mutex);
		return data;
	}

	void close()
	{
		pthread_mutex_lock(&mutex);
		if (running) {
			running = false;
			pthread_cond_broadcast(&cond);
		}
		pthread_mutex_unlock(&mutex);
	}
private:
	list<void *> l;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	bool running;
};

Queue q;

void *publisher(void *ptr)
{
	string in;
	for (;;) {
		cin >> in;
		int *x = new int(atoi(in.c_str()));
		if (*x > 0) {
			q.enqueue(x);
		} else {
			delete x;
			q.close();
			break;
		}
			
	}
}

void *subscriber(void *ptr)
{
	for (;;) {
		int *x = (int *) q.dequeue();
		if (x == NULL) {
			break;
		}
		cout << "Got: " << (*x) << endl;
		delete x;
	}
}

int main()
{
	pthread_t pub, sub;
	pthread_create(&pub, 0, publisher, NULL);
	pthread_create(&sub, 0, subscriber, NULL);
	pthread_join(pub, NULL);
	pthread_join(sub, NULL);
}
