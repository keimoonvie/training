// Implement your own version of a thread pool here.
// Some guidelines:
// - Use a queue.
// - Threads of the pool should be pre-spawned, i.e. they are created, run,
// and then wait for job using dequeue.
// - A 'job' is dispatched by enqueueing it to the queue.
// - Try to think which data structure a job should be.
// - It's best if there is mechanism to gracefully stop the thread pool.

int main()
{
}
