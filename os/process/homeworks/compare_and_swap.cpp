// Compare and swap implements a type of optimistic locking:
// A thread wants to change a value (swap), but the value can only
// be changed if it has not been modified before (compare). If the value
// has been changed, the call from this thread will fail. The
// thread must resolve this by entering a loop, and call compare-and-swap
// repeatedly until success. 
//
// Example usages: lock-free algorithm, every database on earth.
//
// The output of this program should be 10000.
#include <iostream>
#include <pthread.h>

using namespace std;

class Incr
{
public:
	Incr(): val(0)
	{
		pthread_mutex_init(&mutex, NULL);
	}

	virtual ~Incr()
	{
		pthread_mutex_destroy(&mutex);
	}

	int get() const 
	{
		return val;
	}

	// This method should do many things:
	// It is passed the old_value to be compared, the new_value
	// and a reference to set the current_value to.
	// If the comparison fails, it should not modify anything, return false
	// and set the current_value.
	// If the comparison succeeds, it should update the value to new_value,
	// return true, and also set current_value accordingly.
	bool compare_and_swap(int old_value, int new_value, int & current_value)
	{
		pthread_mutex_lock(&mutex);
		if (old_value == val) {
			val = new_value;
			current_value = val;
			pthread_mutex_unlock(&mutex);
			return true;
		}
		current_value = val;
		pthread_mutex_unlock(&mutex);
		return false;
	}
private:
	int val;
	pthread_mutex_t mutex;
};

// Locked code
#define NUM_THREADS 10
#define NUM_ITERS 1000
Incr incr;

void increment()
{
	int x = incr.get();	
	for (;;) {
		if (incr.compare_and_swap(x, x + 1, x))
			break;
	}
}

void *handler(void *ptr) {
	for (int i = 0; i < NUM_ITERS; i++) {
		increment();
	}
}

int main()
{
	pthread_t threads[NUM_THREADS];
	for (int i = 0; i < NUM_THREADS; i++) {
                pthread_create(&threads[i], 0, handler, NULL);
	}
        for (int i = 0; i < NUM_THREADS; i++) {
		pthread_join(threads[i], NULL);
	}
	cout << incr.get() << endl;
}
