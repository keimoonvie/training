// Test and Set is a technique in multithread programming
// that allows only one thread to pass a test, and denies all
// other threads.
//
// Example usages: close file, socket, do cleaning up from
// multiple threads.
//
// The output of this program should be
// only one line of "Hello, world"
#include <iostream>
#include <pthread.h>

using namespace std;

class Hello
{
public:
	Hello()
	{
		pthread_mutex_init(&mutex, NULL);
	}

	virtual ~Hello()
	{
		pthread_mutex_destroy(&mutex);
	}

	bool TestAndSet()
	{
		pthread_mutex_lock(&mutex);
		if (set) {
			pthread_mutex_unlock(&mutex);
			return false;
		}
		set = true;
		pthread_mutex_unlock(&mutex);
		return true;
	}

	void Run()
	{
		cout << "Hello, world" << endl;
	}
private:
	pthread_mutex_t mutex;
	bool set;
};

// Locked code
#define NUM_THREADS 50
Hello hello;

void *handler(void *ptr)
{
	if (hello.TestAndSet())
		hello.Run();
}

int main()
{
	pthread_t threads[NUM_THREADS];
        for (int i = 0; i < NUM_THREADS; i++) {
                pthread_create(&threads[i], 0, handler, NULL);
        }
        for (int i = 0; i < NUM_THREADS; i++) {
                pthread_join(threads[i], NULL);
	}
}

