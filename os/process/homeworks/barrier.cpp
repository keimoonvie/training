// Barrier or latch is a multithreading data structure to "block"
// the program until a certain number of threads has hit it.
//
// Example usages: map-reduce, where all threads must complete the map
// phase first, then the reduce phase is allowed to run. Or it can be used
// to "setup" program state by many threads (connect to database, initialize
// environment...), then the barrier is openned, allow the normal functions to
// run.
//
// A barrier can (and should) be implemented to have timeout. But this feature
// is not needed here.
//
// This program counts the number of each character appears in 5 text files in
// folder 'books'. With each file, a thread is spawned an count independently.
// The result from each thread is then aggregated to produce final output.
#include <iostream>
#include <cstdio>
#include <fcntl.h>
#include <pthread.h>

using namespace std;

class Barrier
{
public:
	Barrier(int num): num(num)
	{
		pthread_mutex_init(&mutex, NULL);
		pthread_cond_init(&cond, NULL);
	}

	virtual ~Barrier()
	{
		pthread_mutex_destroy(&mutex);
		pthread_cond_destroy(&cond);
	}

	void hit()
	{
		pthread_mutex_lock(&mutex);
		num--;
		if (num <= 0) {
			pthread_cond_broadcast(&cond);
		}
		pthread_mutex_unlock(&mutex);
	}

	void await()
	{
		pthread_mutex_lock(&mutex);
		if (num > 0) {
			pthread_cond_wait(&cond, &mutex);
		}		
		pthread_mutex_unlock(&mutex);
	}

private:
	int num;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
};

// Locked code
const char *files[] = {
	"books/1.txt",
	"books/2.txt",
	"books/3.txt",
	"books/4.txt",
	"books/5.txt"
};

int char_maps[5][256];
int thread_nums[5] = {0, 1, 2, 3, 4};
Barrier barrier(5);

void *handler(void *ptr)
{
	int num = * (int *) ptr;
	char filename[20];
	sprintf(filename, "books/%d.txt", num + 1);
	int fd = open(filename, O_RDONLY);
	unsigned char buffer[1024];
	for (;;) {
		int n = read(fd, buffer, 10);
		if (n <= 0) {
			break;
		}
		for (int i = 0; i < n; i++) {
			char_maps[num][buffer[i]]++;
		}
	}	
	close(fd);
	barrier.hit();
}

int main()
{
	for (int i = 0; i < 5; i++)
		for (int j = 0; j < 256; j++)
			char_maps[i][j] = 1;
	pthread_t threads[5];
	for (int i = 0; i < 5; i++) {
                pthread_create(&threads[i], 0, handler, &thread_nums[i]);
        }
	barrier.await();
	int result[256];
	for (int i = 0; i < 256; i++) {
		int total = 0;
		for (int j = 0; j < 5; j++)
			total += char_maps[j][i];
		result[i] = total;
	}		
	for (int i = '0'; i < '9'; i++)
		printf("%c: %d\n", i, result[i]);
	for (int i = 'a'; i < 'z'; i++)
		printf("%c: %d\n", i, result[i]);
	for (int i = 'A'; i < 'Z'; i++)
		printf("%c: %d\n", i, result[i]);
}
