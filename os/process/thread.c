#include <stdio.h>
#include <pthread.h>
#include <sys/prctl.h>

#define THREAD_NUM 10

int nums[THREAD_NUM];

void * handler(void *ptr)
{
	int num = * (int *) ptr;
	char name[20];
	sprintf(name, "%s %d", "Thread", num);
	prctl(PR_SET_NAME, name, 0, 0, 0);
	printf("Inside thread %d\n", num);
	sleep(30);
	printf("Thread %d exiting\n", num);
}

int main()
{
	// Creating thread
	pthread_t threads[THREAD_NUM];
	int i = 0;
	for (i = 0; i < THREAD_NUM; i++) {
		nums[i] = i;
		pthread_create(&threads[i], 0, handler, &nums[i]);
	}
	// Join
	for (i = 0; i < THREAD_NUM; i++)
		pthread_join(threads[i], NULL);
}
