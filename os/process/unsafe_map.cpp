#include <cstdio>
#include <iostream>
#include <string>
#include <cstdlib>
#include <sparsehash/sparse_hash_map>
#include <boost/lexical_cast.hpp>

using namespace std;

class Person
{
public:
	Person(const string & name, int age): name(name), age(age) {
	}

	inline const string & get_name() const {
		return name;
	}

	inline int get_age() const {
		return age;
	}
private:
	string name;
	int age;
};

typedef Person* person_ptr;
typedef google::sparse_hash_map<int, person_ptr> person_map_t;

person_map_t person_map;

void add_person(int id, const string & name, int age)
{
	person_ptr person = new Person(name, age);
	person_map[id] = person;
}

person_ptr get_person(int id)
{
	person_map_t::iterator it = person_map.find(id);
	if (it == person_map.end())
		return NULL;
	return it->second;
}

void delete_person(int id)
{
	person_map_t::iterator it = person_map.find(id);
	if (it == person_map.end())
		return;
	person_ptr person = it->second;
	person_map.erase(id);
	delete person;
}

void *handler(void *ptr)
{
	for (int i = 0; i < 1000; i++) {
		add_person(i, boost::lexical_cast<string>(i), i);
	}
	for (int i = 0; i < 1000; i++) {
		person_ptr person = get_person(i);
		if (person != NULL) {
			printf("Person: %d, %s, %d\n", i, person->get_name().c_str(), person->get_age());
		}
	}
	for (int i = 0; i < 1000; i++) {
		delete_person(i);
	}
}

#define NUM_THREADS 2

int main() {
	person_map.set_deleted_key(-1);
	pthread_t threads[NUM_THREADS];
	for (int i = 0; i < NUM_THREADS; i++) {
		pthread_create(&threads[i], 0, handler, NULL);
	}
	for (int i = 0; i < NUM_THREADS; i++) {
		pthread_join(threads[i], NULL);
	}
}
